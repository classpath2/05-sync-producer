package io.classpath.kafka.examples.config;

public class AppConfig {
    public final static String applicationID = "SyncProducer";
    public final static String bootstrapServers = "<broker-1>:9092,<broker-2>:9092,<broker-3>:9092";
    public final static String topicName = "sync-producer";
    public final static int numEvents = 100;
}